+++
title = "Weekly GNU-like Mobile Linux Update (31/2022): Automated Phone Testing for postmarketOS and various little updates"
date = "2022-08-07T21:00:00Z"
draft = false
[taxonomies]
tags = ["Nemo Mobile","Manjaro ARM","Maemo Leste","Librem 5","PinePhone Keyboard","Ubuntu Touch","Tablets",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author = "Peter"
+++
Blog posts about the progress of Nemo Mobile and Manjaro ARM and this and that. ;-)

<!-- more -->
_Commentary in italics._


### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#55 Wallpapers & Screenshots](https://thisweek.gnome.org/posts/2022/08/twig-55/). _Nice progress!_
* Rust in Peace: [Aarch64 for GNOME Nightly apps](https://blogs.gnome.org/alatiera/2022/08/04/aarch64-for-gnome-nightly-apps/). _Easier app testing, yay!_
* Federico's Blog: [Paying technical debt in our accessibility infrastructure - Transcript from my GUADEC talk](https://viruta.org/paying-technical-debt-transcript.html).
* Philip Withnall: [Mini-GUADEC 2022 Berlin: retrospective](https://tecnocode.co.uk/2022/08/05/mini-guadec-2022-berlin-retrospective/).
##### Releases
* [Phoc 0.21 has been released](https://gitlab.gnome.org/World/Phosh/phoc/-/releases/v0.21.0).

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: Easier Samba sharing setup](https://pointieststick.com/2022/08/05/this-week-in-kde-easier-samba-sharing-setup/).
* KDE Community: [KDE Plasma 5.25.4, Bugfix Release for August](https://kde.org/announcements/plasma/5/5.25.4/).

#### Nemo Mobile
* Nemo Mobile: [Nemomobile in July 2022](https://nemomobile.net/pages/nemomobile-in-july-2022/). _Nice progress!_

#### Distributions
* Manjaro Blog: [July 2022 in Manjaro ARM](https://blog.manjaro.org/july-2022-in-manjaro-arm/). _Nice update, Dan!_


### Worth noting
* [Chris Vogel: "I just made two exciting discoveries using my #Li…" - Librem Social](https://social.librem.one/@chrichri/108754910548068030)
* /u/QushAes: [Am I the only one disappointed by the software situation of the Pinephone after almost 2 years of it being out? : PinePhoneOfficial](https://www.reddit.com/r/PinePhoneOfficial/comments/weler6/am_i_the_only_one_disappointed_by_the_software/). _I am so tired of reading complainy posts, but there are some good replies in that thread._
* amosbatto on Purism forums: [Will Linux phones get decent processors in the future?](https://forums.puri.sm/t/will-linux-phones-get-decent-processors-in-the-future/17956).
* They want your [Vote for an official case for the L5 - Librem / Phones (Librem 5) - Purism community](https://forums.puri.sm/t/vote-for-an-official-case-for-the-l5/17987).


### Worth reading
#### Automated testing
* Martijn Braam: [Automated Phone Testing pt.1](https://blog.brixit.nl/automated-phone-testing-pt-1/). _Scale!_

#### PinePhone Keyboard Findings
* megi's PinePhone Development Log: [Pinephone keyboard bugs and new findings](https://xnux.eu/log/#072).

#### Maemo Leste
* Camden Bruce: [Maemo Leste might be right for your PinePhone](https://medium.com/@camden.o.b/maemo-leste-might-be-right-for-your-pinephone-f0345485e8b1). _Nice post about a great project!_

#### First steps
* Buck Ryan: [Getting started with the PinePhone Pro](https://www.buckryan.com/blog/getting-started-with-pinephone-pro/).

#### Quicker delivery
* Purism: [Librem 5 USA At Shipping Parity: New Orders Ship Within 10 Business Days](https://puri.sm/posts/librem-5-usa-at-shipping-parity-new-orders-ship-within-10-business-days/).

### Worth listening
* postmarketOS podcast: [#21 INTERVIEW: Sebastian Krzyszkowiak (of Phosh, Librem 5 Fame)](https://cast.postmarketos.org/episode/21-Interview-Sebastian-Krzyszkowiak-Phosh-Librem5/). _I bet this one is a great one!_
* [GLN025 - Android Launcher, Faster Faster Faster, Linux Smartphones, Kopano](https://gnulinux.ch/gln025-podcast). _You'll need to understand German to enjoy listening to this._

### Worth watching
#### Maemo Leste
* Camden Bruce: [Maemo Leste demo on the PinePhone](https://www.youtube.com/watch?v=TlDiHf5zdRs).

#### Installing postmarketOS
* Vu Android: [[Galaxy A72] Install postmarketOS via TWRP | Boot linux on Galaxy A72 | Without losing your data](https://www.youtube.com/watch?v=bMPmxBmTi3k).

#### Tablets
* Explaining Computers: [Linux for an x86 Tablet](https://www.youtube.com/watch?v=UaCwLjIf3sU).

#### Conference Talks about Ubuntu Touch
* MCH2022: [2022 - UBports: Imagine a phone that does everything you expect and nothing you don't.](https://www.youtube.com/watch?v=-7nu2tmr-lQ).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

24
