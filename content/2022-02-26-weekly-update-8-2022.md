+++
title = "Weekly Update (8/2022): Phosh 0.16.0, Nemo Mobile's February Report and adding Kodi to Megi's multi boot image"
date = "2022-02-26T12:20:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","Librem 5","Ubuntu Touch","Cutie Pi","Phosh","GNUnet messenger","Nemo Mobile","Kodi","Multi Boot Image","DanctNIX Mobile", "Arch Linux ARM", "Steam Deck", "PinePhone Pro",]
categories = ["weekly update"]
authors = ["peter"]
+++

Another boring week with just a few releases, I even had to reach for Steam Deck content to fill this one up.

<!-- more -->

_Commentary in italics._

### Hardware enablement
* xnux.eu log: [Pinephone Pro – Debugging Type C port issues](https://xnux.eu/log/#061). _If you have issues with USB C on your PinePhone Pro, this is how to obtain good logs._
* Martin Kepplinger for Purism: [Purism and Linux 5.17](https://puri.sm/posts/purism-and-linux-5-17/)

### Software news
#### GNOME ecosystem
* [Phosh 0.16.0 has been released](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.16.0), featuring number shuffling on the lockscreen keypad and style refreshes. _Make sure to skim through the detailed changes, there's some "eases GTK4 port" in there. Exciting! [And then there's a last minute development](https://twitter.com/GuidoGuenther/status/1497544780963885058)..._
* [GNOME Calls seen another pre-release, 42.rc.0, with just a few more fixes](https://gitlab.gnome.org/GNOME/calls/-/tags/42.rc.0).
* Martín Abente Lahaye: [Portfolio 0.9.13](https://blogs.gnome.org/tchx84/2022/02/20/portfolio-0-9-13/). _A really nice file manager!_
* This Week in GNOME: [#32 Security Issues](https://thisweek.gnome.org/posts/2022/02/twig-32/). _I am puzzled by that puzzle. More importantly, I fully support the message by the TWIG team – I tried to write a similar message as an editorial for this week, but did not manage to come up with something good enough._
* Jacki: [GNUnet Messenger API: February](https://thejackimonster.blogspot.com/2022/02/gnunet-messenger-api-february.html). _Looking forward to the release!_
* GNOME: [GNOME 42.beta released](https://mail.gnome.org/archives/devel-announce-list/2022-February/msg00002.html). _With GTK4 and libadwaita properly landing in GNOME, this release is going to affect mobile Linux a lot too as apps are being ported over._


#### Plasma/Maui ecosystem
* flyingcakes: [Status update: Completing a milestone - post #3](https://snehit.dev/posts/kde/sok-2022/completing-a-milestone/). This is about flatpak-ing up KDE apps._
* Nate Graham: [This week in KDE: Bugfixing Plasma 5.24](https://pointieststick.com/2022/02/25/this-week-in-kde-bugfixing-plasma-5-24/).

#### Ubuntu Touch
* UBports: [And... another biweekly edition of the UBports news letter is here!](https://ubports.com/blog/ubports-news-1/post/and-another-biweekly-edition-of-the-ubports-news-letter-is-here-3836).
* UBports: [Press release: UBports foundation releases version OTA-22, adds video calls to Ubuntu Touch](https://ubports.com/blog/ubports-news-1/post/press-release-ubports-foundation-releases-version-ota-22-adds-video-calls-to-ubuntu-touch-3837).

#### Nemo Mobile
* Nemo Mobile: [Nemomobile in February 2022](https://nemomobile.net/pages/nemomobile-in-february-2022/). _Nice progress! Make sure to read the entire post!_


#### Distro releases
* Danct12 has [released another round of images](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20220222) for his Arch Linux ARM-based distribution. _The biggest change is in the repo setup: It's all one repo now (which is great news for everyone with a touch screen device running Arch Linux ARM, as it makes getting mobile friendly packages a lot easier), and there's a testing repo if you're brave and want to help!_

#### Alternative Modem Firmware

### Worth reading

#### How To Multi Distro image and Kodi
* xnux.eu log: [Adding LibreELEC.tv to an existing Pinephone multi-distro image](https://xnux.eu/log/#062).

#### Networking and postmarketOS infrastructure
* Martijn Braam: [Going IPv6 only](https://blog.brixit.nl/going-ipv6-only/).

#### Time is scarce
* Jeff: [How long does it take to create a website? (and why your FLOSS project doesn’t need one)](https://fortintam.com/blog/how-long-does-it-take-to-write-and-design-a-website/).

#### Proper Software Releases
* Nibble Stew: [Please provide tarball releases of your projects](https://nibblestew.blogspot.com/2022/02/please-provide-tarball-releases-of-your.html).

#### Proprietary apps on PinePhone
* Mr Potter: [NoteCase Pro on Pinephone](https://mrpotter.us/posts/pinephone-notecase-pro/).

#### Mobile Gaming on a Linux handheld
* Liliputing: [Steam Deck Review roundup: A promising work in progress](https://liliputing.com/2022/02/steam-deck-review-roundup-a-promising-work-in-progress.html).
* Phoronix: [For Linux Enthusiasts Especially, The Steam Deck Is An Incredible & Fun Device](https://www.phoronix.com/scan.php?page=article&item=steam-deck-steamos-linux&num=1)

### Worth watching

#### Another Linux Tablet
* PizzaLovingNerd: [CutiePi Review - The Raspberry Pi Powered iPad Mini](https://www.youtube.com/watch?v=p7-yJZyk_ZE). _Great video about an interesting device!_

#### Ubuntu Touch
* animo207: [Proof of Concept of Immersive Mode in Lomiri | Ubuntu Touch](https://www.youtube.com/watch?v=Dtaj0fedWuI).

#### Nemo Mobile
* Jozef Mlich: [glacier-calendar progress](https://www.youtube.com/watch?v=SBw0ZnFlMgQ).

#### postmarketOS
* Data Portability and Services Incubator: [PostmarketOS project presentation at DAPSI Final Event #2](https://www.youtube.com/watch?v=G4kHjUopoWw).

#### PinePhone Pro Gaming
* @FOSSingularity: [Video preview of my [Furthest into development] video game](https://twitter.com/FOSSingularity/status/1496182870716596228)

#### PinePhone accessories
* LINMOB.net: [PinePhone (Pro) External Batteries: PinePhone Keyboard vs. 8500 mAh S 20 Ultra Battery case](https://tilvids.com/w/w3qXbr31SE21jGxnM8A1r7). _This is about the battery case we had in the last Weekly Update._

#### Kodi on PinePhone
* sadfwesv: [Running LibreELEC.tv on original Pinephone](https://www.youtube.com/watch?v=E79XSCq5z6Y). _Nice demo by Megi!_

#### PinePhone SDR
* cemaxecutor: [DragonOS Focal SigDigger and Android SDR++ connected to PinePhone SDR Server (RTLSDR, HackRF)](https://www.youtube.com/watch?v=srTz363bPYc). _I'll be honest: I don't understand this SDR stuff._

#### Shorts
* veer66: [Ubuntu Touch 16.04 OTA 22 on Redmi Note 7](https://www.youtube.com/shorts/vIVlTbYS8Aw).


### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
