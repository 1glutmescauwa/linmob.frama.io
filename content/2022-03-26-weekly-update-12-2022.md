+++
title = "Weekly Update (12/2022): GNOME 42, Phosh 0.17.0, Phoc 0.13.0, Mepo 0.4 and Capyloon on Linux Phones!"
draft = false
[taxonomies]
tags = ["PinePhone","Librem 5", ]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author = "Peter"
+++

A new PinePhone Modem Firmware release, updates on Nemo Mobile and GNUnet Messenger progress, Calls on the Poco F1, and a bunch of gaming centric PinePhone videos! And do try Capyloon on your Mobian-running Librem 5 or PinePhone Pro!

<!-- more -->

_Commentary in italics._

### Hardware enablement
#### Snapdragon 845 call audio
* [Call audio rooting now works in postmarketOS edge on the Xiaomi Poco F1!](https://twitter.com/joelselvaraj95/status/1478971761127350275)
#### Display out on PinePhone Pro
* Megi: [Some finer points about Alt-DP support in mainline kernel](https://xnux.eu/log/#063).

### Software progress

#### Firmware
* Biktorgj [released version 0.6.1](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.6.1) of his open source modem firmware, making it even more awesome!

#### GNOME ecosystem
* [GNOME 42 has been released](https://release.gnome.org/42/). _Many apps were ported to GTK4/libadwaita in the process, and some of them have become convergent apps in the process, e.g. [Polari](https://linmob.uber.space/applist.html#polari). A great release!_
* Phosh 0.17.0 has been [announced](https://social.librem.one/@agx/108017540758161102) and [released](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.17.0), featuring a mobile data indicator and portal support.
* Phoc 0.13.0 has also been [released](https://gitlab.gnome.org/World/Phosh/phoc/-/releases#v0.13.0). _Since gnome.org is undergoing maintenance currently, I have no idea what changed._
* This Week in GNOME: [#36 Forty-two!](https://thisweek.gnome.org/posts/2022/03/twig-36/). _Nice updates!_
* Christian Hergert: [Rendering Text with Glyphy](https://blogs.gnome.org/chergert/2022/03/20/rendering-text-with-glyphy/).


#### Plasma/Maui ecosystem
* Felipe Kinoshita: [Coast, a little map viewer](https://fhek.gitlab.io/writings/2022-03-22-map-viewer/). _Nice!_
* Qt blog: [Qt Creator 7 released](https://www.qt.io/blog/qt-creator-7-released).
* Volker Krause: [Wikidata Data Reuse Days 2022 Recap](https://www.volkerkrause.eu/2022/03/26/wikidata-data-reuse-days-2022-recap.html).

#### Apps
* [Mepo 0.4 was released](https://lists.sr.ht/~mil/mepo-devel/%3Ca7cec9f4-30fd-4a5c-afd0-ff46c4b7406d%40www.fastmail.com%3E). _It's a great maps app, make sure to check out the video linked below!_
* If you've been looking for a WhatsApp Web client for your Mobile Linux device, [you're now in luck](https://twitter.com/hadrianweb/status/1507362672446259205#m)!

#### Sailfish OS
* flypig: [Sailfish Community News, 24 March, Games](https://forum.sailfishos.org/t/sailfish-community-news-24-march-games/10779).

#### UBports
* Jeroen Baten for UBports: [Your biweekly UBports news with a (buggy) new design](http://ubports.com/blog/ubports-news-1/post/your-biweekly-ubports-news-with-a-buggy-new-design-3842).
* UBports: [Ubuntu Touch Q&A 117](https://ubports.com/de/blog/ubports-blogs-nachrichten-1/post/ubuntu-touch-q-a-117-3841). _Transcription and Audio only of last weeks Q&A._

#### Nemo Mobile
* Nemo Mobile: [Nemomobile in March/2022](https://nemomobile.net/pages/nemomobile-in-march-2022/). _Great work!_

#### Capyloon
* Capyloon [announced](https://twitter.com/capyloon/status/1507210332153868288/) and [published deb packages](https://capyloon.org/linuxphones.html) to be run atop of Mobian on the Librem 5 and PinePhone Pro. A [post on the Purism forums](https://forums.puri.sm/t/capyloon-now-available-for-the-librem-5/16784/6) might help understand the goals and scope of the Capyloon project better. _I've shared a [few photos of Capyloon running on the PinePhone Pro on Twitter](https://twitter.com/linmobblog/status/1507406190451609611#m). I really wish I could just use it as a mobile browser on Phosh (or Plasma or Sxmo or...), but understand that this against the projects goals)._

### Worth noting
* A [mockup detailing the visual future of Phosh](https://teddit.net/r/gnome/comments/tjyz1h/mockup_mobile_shell_visuals_by_tobias_bernard/) appeared!

### Worth reading

#### Interviews
* FSFE: [Interview with Plasma Mobile developer Bhushan Shah](https://fsfe.org/news/2022/news-20220323-01.html).

#### Librem 5 First Impressions
* James Stanley: [Librem 5: first impressions](https://incoherency.co.uk/blog/stories/librem5-first-impressions.html). _Nice read! BTW: I [received my original pre-order yesterday](https://fosstodon.org/web/@linmob/108018076434351116)._

#### GNUnet Messenger
* Jacki: [GNUnet Messenger API: March](https://thejackimonster.blogspot.com/2022/03/gnunet-messenger-api-march.html). _Great progress!_

#### Rants
* Martijn Braam: [The end of the nice GTK button](https://blog.brixit.nl/the-end-of-the-nice-gtk-button/). _Nice rant - I am not a fan of flat design either (although, in all fairness, the new libadwaita style is definitely not iOS 7-level bad. Apparently Martijn struck a chord - it went viral on [HackerNews](https://news.ycombinator.com/item?id=30795846), and been actively discussed on [r/linux](https://teddit.net/r/linux/comments/tn6v9z/the_end_of_the_nice_gtk_button/) and [r/GNOME](https://teddit.net/r/gnome/comments/tn8why/the_end_of_the_nice_gtk_button/)!_


#### A Kernel of Progress
* CNX Software: [Linux 5.17 release – Main changes, Arm, RISC-V, and MIPS architectures](https://www.cnx-software.com/2022/03/21/linux-5-17-release-main-changes-arm-risc-v-and-mips-architectures/). __

#### Capyloon
* Liliputing: [Capyloon’s web-based OS is now available for mainline Linux phones (PinePhone Pro and Librem 5)](https://liliputing.com/2022/03/capyloons-web-based-os-is-now-available-for-mainline-linux-phones-pinephone-pro-and-librem-5.html).
* CNX Software: [Capyloon mobile Web-based OS works on Pinephone Pro, Librem 5, Pixel 3a](https://www.cnx-software.com/2022/03/26/capyloon-mobile-web-based-os-works-on-pinephone-pro-librem-5-pixel-3a/).

### Worth watching

_LibrePlanet took place and had at least one relevant [talk about Sxmo](https://libreplanet.org/2022/speakers/#5624), but from what I could gather, recordings are still being processed. I'll add links once the videos are out (if I should forget, feel free to ping me!)._

#### Mepo - zig + SDL = Great Maps
* Miles Alan: [Mepo 0.4](https://media.lrdu.org/mepo_demos/mepo_demo_0.4.webm). _Great video, thanks for creating it and sending it in, Miles!_

#### PinePhone Pro and Tow-Boot
* Ambro's: [How to Install Tow-Boot and Arch Linux on the Pinephone Pro](https://www.youtube.com/watch?v=hwALn0ALYwk).
* Canadian Bitcoiners: [PinePhone Pro Update - Suspend / Wake Up Working Now Thanks To Tow-Boot!](https://www.youtube.com/watch?v=euqV01aN8NE).

#### PinePhone Pro Unboxings
* socketwench: [PinePhone Pro Unboxing](https://www.youtube.com/watch?v=yMzwMwfas0k). _I am glad that they managed to successfully get it working later!_

#### GNUnet Messenger on PinePhone
* Tobias Frisch: [GNUnet Messenger: March 2022](https://www.youtube.com/watch?v=yAvHoZxreiE). _I've seen many, many apps, but I am pretty sure that this is the best, most feature rich GTK3/libhandy messaging UX I have seen.._

#### PinePhone gaming
* A Sicilian in Sweden: [explosive-c4 gameplay on PinePhone](https://www.youtube.com/watch?v=oBcFPTqldt8).
* Supositware: [ioquake3 on the PinePhone with keyboard case](https://www.youtube.com/watch?v=Xwz9hG-1x_M).
* Supositware: [Urban Terror on the PinePhone](https://www.youtube.com/watch?v=7iHHFAHMg_0).
* Supositware: [NZ:P on the PinePhone](https://www.youtube.com/watch?v=cCisdUbQxIk).

#### Something something Gameboy PinePhone
*  Nephitejnf: [How to use the FlashGBx CLI... on the pinephone](https://www.youtube.com/watch?v=UXdwlYTFOVs).

#### Snapdragon 845 Linux Phones
* LINMOBnet: [The fastest mainline Linux Phone: Pocophone F1](https://www.youtube.com/watch?v=KtfTJbLiYfg). _Some more notes on the Poco F1: Plasma Mobile feels really great on this hardware. Sadly, unlike Manjaro, the Maliit keyboard does not work with GTK apps (or Firefox) on postmarketOS (OOTB), meaning I'll likely have to switch to Phosh to use the apps I usually run on my Linux Phones. I've used it lightly since making the video, and battery life is solid (I should get a day out of this, even with more apps and use). I'll definitely report on how things are going in a blog post and/or video._

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
