+++
title = "LinBits 32: Weekly Linux Phone news / media roundup (week 6)"
aliases = ["2021/02/14/linbits32-weekly-linux-phone-news-week6.html", "linbits32"]
author = "Peter"
date = "2021-02-14T22:00:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "PineTalk", "PineTab", "Phosh", "Portfolio", "Sxmo", "postmarketOS"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Phosh 0.8.1, Calls 0.3.0, Sxmo 1.3.0 and more! _Commentary in italics._
<!-- more -->

### Software development and releases

#### Apps
* Phosh 0.8.1 [is out](https://social.librem.one/@agx/105719264028836408), as the number indicates it's mostly a bug fix release. Head over to the [full release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.8.1) for the details. 
* Calls 0.3.0 has been released, adding a few features, translations and bug fixes. Go read the [full changelog](https://source.puri.sm/Librem5/calls/-/commit/82275c7feab9f283d5c08c267a336898eb761ef4).
* [Portfolio 0.9.9 has been released](https://github.com/tchx84/Portfolio/blob/master/CHANGELOG.md), adding support for the org.freedesktop.FileManager1 interface among other things.
* A new version of Authenticator, powered by Rust and GTK4 [is out](https://twitter.com/bil_moussaoui/status/1356063423549874178). _Sadly it did not work for me when I tried the Flatpak on postmarketOS._

#### Distributions
* [Arch Linux ARM/DanctNIX mobile has seen a new release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases), incorporating almost all the new releases mentioned above and more.
* Sxmo 1.3.0 has been released, delivering improvements to dmenu, which now reacts on release of touch and more. Read the full [release notes](https://lists.sr.ht/~mil/sxmo-announce/%3C20210208124655.jzvioyzsbi5huh7d%40worker.anaproy.lxd%3E).

### Worth noting
* [Dylan Van Assche announces great news coming to the postmarketOS](https://twitter.com/DylanVanAssche/status/1360306388648431616) on the PinePhone: Ringing immideately, even when in suspend. _This should be the relevant [merge request](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/1931)._
* [Manjaro are preparing something that looks interesting!](https://twitter.com/ManjaroLinux/status/1360994902025392130)

### Worth reading 
* Mobian Blog: [Issues and hurdles when performing the initial upgrade](https://blog.mobian-project.org/posts/2021/02/09/pam_issue/). _If you ordered a Mobian CE, make sure to read this post! (I was going to make a video about this, but I did not keep the screen alive. Turns out, even if the phone locks up; when you just wait a long time, rebooting the hard way can be safe too.)_
* Phoronix: [Sailfish OS 4.0.1 No Longer Support The Jolla Phone But Has Many Other Improvements](https://www.phoronix.com/scan.php?page=news_item&px=Sailfish-OS-4.0.1-Released). _Nice new features coming to Sailfish OS._
* Linux Smartphones: [Sxmo 1.3.0 brings usability, performance improvements to this simple, geeky Linux phone UI](https://linuxsmartphones.com/sxmo-1-3-0-brings-usability-performance-improvements-to-this-simple-geeky-linux-phone-ui/). _Nice write-up by Brad on the the new Sxmo release. Make sure to watch his video, which is linked below, too._
* Purism: [OpenPGP in Your Pocket](https://puri.sm/posts/openpgp-in-your-pocket/). _Guess I need to get this firmware on to my Librem 5._
* Lup Yuen Lee: [PineCone BL602 talks SPI too!](https://lupyuen.github.io/articles/spi). _Turns out: You can connect a display to that Risc-V WiFi-thingy!_


### Worth listening
* PineTalk 002: [New Accessories and UBports with Dalton Durst](https://www.pine64.org/2021/02/12/002-new-accessories-and-ubports-with-dalton-durst/). _Creating this episode was a lot of fun, I hope you like it!_


### Worth watching

* Glitchy Soup: [Pinephone (Mobian community edition) GUI and camera test](https://www.youtube.com/watch?v=W7nQdsG4SjI). _Nice video._
* AntonMadness: [Soldering with the PinePhone](https://www.youtube.com/watch?v=g3blKJGKQ1k). _Yes, this is the PinePhone serving as a power source for a USB-C powered soldering iron. I don't think that this is something you should too, but apparently this works at least once._
* paralin: [KDE and GentooLTO on Pinephone](https://www.youtube.com/watch?v=xeGvdLaZCNg). _This is the KDE Plasma Desktop, running on a PinePhone. Interesting!_
* The Linux Experiment: [Is GNOME ready to tackle the SMARTPHONE ? A tour of Phosh on the PinePhone](https://www.youtube.com/watch?v=0T8SfGNpBfA). _Interesting user interface critique._
* Liliputing: [PinePhone running Sxmo 1.3.0 (Simple X Mobile)](https://www.youtube.com/watch?v=W91B5ugRbQ4). _I am so happy that Brad did this video so I don't._
* Linux Lounge: [A Look At LuneOS On The PinePhone](https://odysee.com/@LinuxLounge:b/a-look-at-luneos-on-the-pinephone:d). _Nice video on LuneOS, although I sadly did not see much progress to way back when I tried it._
* Privacy & Tech Tips: [Howto: Put BlackArch Pentesting Linux On Pinephone!](https://odysee.com/@RTP:9/howto-put-blackarch-pentesting-linux-on:3). _Great video!_
* Privacy & Tech Tips: [Pine64 Pinetab (Linux Tablet) First Impressions- Nice!](https://odysee.com/@RTP:9/pine64-pinetab-linux-tablet-first:7). _If you're curious about the PineTab..._
* Privacy & Tech Tips: [Dear Newbz: Are You Ready For A Pinephone? Depends.](https://odysee.com/@RTP:9/is-pinephone-ready-to-be-your-daily:a). _This is an important video for everybody who is not really sure, whether PinePhone is for them._
* Leszek Lesner: [SailfishOS 4.0 EA - What's new?](https://www.youtube.com/watch?v=dQToX-vM1K0). _Informative video!_
* Linux Lounge: [How To Install Replicant (A Fully FOSS Android Distribution)](https://odysee.com/@LinuxLounge:b/how-to-install-replicant-a-fully-foss:d). _If you want to try the variant of Android that is the most FLOSS, this video is a good starting point._
* UBports: [Ubuntu Touch Q&A 94](https://www.youtube.com/watch?v=2JpVH3CIwcA). _Highlights include: OTA 16 upcoming, Debian packaging progress (slow!)._

#### Unboxing corner
* TalkNTech: [Pine64 Pinephone Manjaro KDE CE (linuxphone) unboxing + overview](https://www.youtube.com/watch?v=ejlP4_bZqZQ&t=3s), [part 2](https://www.youtube.com/watch?v=GSRH_ZGWCvs).
* Nephitejnf: [Pinephone Unboxing and First Impressions](https://www.youtube.com/watch?v=TdY52r5B9u8)
* Jolla-Devices: [Pinephone unboxing and magnets // J-D inbox February](https://www.youtube.com/watch?v=smDJRCa_DLc). _Another late Manjaro CE unboxing._

### Stuff I did

#### Content

I made no video. I published one [blog post about scaling in Phosh](https://linmob.net/2021/02/13/pinephone-setup-scaling-in-phosh.html), and [it's not really ready yet](https://fosstodon.org/@linmob/105729625011845838).

#### Random

I installed postmarketOS Edge with Phosh on my Librem 5 Evergreen last week, and reported on the battery life I experienced during the past week:
* [Day 1](https://fosstodon.org/@linmob/105696348333072976),
* [Day 2](https://fosstodon.org/@linmob/105703359787356807),
* [Day 3](https://fosstodon.org/@linmob/105711781935156666),
* [Day 4](https://fosstodon.org/@linmob/105713512304727231),
* [Day 5](https://fosstodon.org/@linmob/105719415459140758).

I have nuked that install today and tried PureOS Byzantium tonight (it's really not ready yet), and might go Mobian next. Stay tuned!

Meanwhile, my [Fedora on PinePhone video (the Odysee upload)](https://odysee.com/@linmob:3/fedora-on-the-pinephone-pipewire-calling:1) went sort of viral on ["Hacker News"](https://news.ycombinator.com/item?id=26080207).

I also [published my first APKBUILD](https://fosstodon.org/@linmob/105723460572409668).

#### LINMOBapps

This week I added [just one app](https://fosstodon.org/@linmob/105703922646357593) to LINMOBapps, [and some maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). I also added some more [PKGBUILDs](https://framagit.org/linmobapps/linmobapps.frama.io/-/tree/master/PKGBUILDs). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 

