+++
title = "Getting started with the KDE CE PinePhone"
aliases = ["2021/01/23/getting-started-with-the-kde-ce-pinephone.html"]
author = "Peter"
date = "2021-01-23T17:00:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["Pine64", "PinePhone", "KDE", "Plasma Mobile", "Getting started", "Tutorials"]
categories = ["howto", "videos"]
+++

_This is an accompanying blog post to yesterdays video._

<!-- more -->

<p style="padding: 5px; background-color: #FFFFE0; border: 2px solid #FFFF00;">If your install is visually broken after the upgrade (bottom and top bar missing), run <code>rm ~/.config/plasma-org.kde.plasma.phoneshell-appletsrc</code> after checking <a href="https://forum.pine64.org/showthread.php?tid=12862&pid=88363#pid88363">this thread on the PINE64 Forums</a>. Reboot after this, and the issue should be gone.</p> 

A week ago, I started to prepare what was just supposed to be another video on the current state of Manjaro Plasma Mobile[^1], and figured I would use the factory image for that. I used an image on Manjaro's [OSDN](https://osdn.net/projects/manjaro-arm/storage/pinephone/plasma-mobile/factory/Manjaro-ARM-plasma-mobile-pinephone-20.12-factory-20201207.img.xz), put it on SD and tried to upgrade it. I ran into problems, discover crashed, the pacman db remained locked after reboot, so I tried to resolve them with pacman and would end up with a broken install. I was shocked, and talked to [Bhushan Shah](https://blog.bshah.in/) about it, who provided me with the actual factory image.

So I figured, ok, let’s take that actual factory image and demo that whole process, so this does not happen to the new PinePhone owners that are receiving their KDE Community Edition PinePhones. 

Having witnessed multiple reports of "the battery says it's charged, but when I unplug it the phone dies" after shipment of the previous CE's, I decided to include a section on this basic thing into the video. Then: Updating. It took a long time, I had dinner while the phone downloaded and applied the update after the second restart &mdash; the phone has to be restarted twice, as Discover first has to update the distributions keyring with the first restart, which is necessary to determine that the downloaded packages are authentic. After that, the package updates can now be applied, for which the phone is restarted again. It took almost 15 minutes, so be patient and make sure that the PinePhone is plugged into power during this process.

After that, you can start using the PinePhone. In my video, I did not do the best job to describe that, which is why I linked to a previous video for further explanation.[^2] So let's have a short list, to explain what the pre-installed apps do:

* [Angelfish](https://invent.kde.org/plasma-mobile/angelfish) is Plasma Mobiles Qt WebEngine based web browser. 
* [Buho](https://invent.kde.org/maui/buho) is a note taking app, that is fairly advanced and supports organizing the notes in books, has a way to sync etc,
* [Calculator](https://invent.kde.org/plasma-mobile/kalk) is just what its name suggests,
* [Calindori](https://apps.kde.org/en/calindori) is a touch friendly calendar application. It does not support sync yet, but [this blog post](https://dimitris.cc/kde/2020/12/30/Online_Calendars.html) has instructions on how you "hack" around that,
* [Clock](https://invent.kde.org/plasma-mobile/kclock) is (again) just what its name suggests,
* [Discover](https://apps.kde.org/en/discover) is the place where you can find and install apps and keep your OS up to date,
* [Index](https://invent.kde.org/maui/index-fm) is a nice convergent file manager,
* [KDE Connect](https://kdeconnect.kde.org/) is a tool to connect your PinePhone to your desktop (which can run Linux, Windows or macOS) or your other Phone, if that runs Android or Sailfish OS. You can share files and notifications, control media playback and more.
* [Nota](https://invent.kde.org/maui/nota) is a multi-platfrom text editor,
* [Keysmith](https://invent.kde.org/utilities/keysmith) is a client for time- and hash-based-OTP, so that you can use you PinePhone as a second factor,
* [Kirigami Gallery](https://invent.kde.org/sdk/kirigami-gallery) unless you want to build applications for Plasma Mobile, you won't need this app,
* [KoKo](https://invent.kde.org/graphics/koko) is an image viewer, it defaults to ~/Pictures.
* [Kongress](https://invent.kde.org/utilities/kongress) is a companion application for conferences, you can currently use it to plan your [FOSDEM 2021](https://fosdem.org/2021/),
* [KTrip](https://invent.kde.org/utilities/ktrip) is a public transport assistant which allows to query journeys for a wide range of countries/public transport providers,
* [Megapixels](https://git.sr.ht/~martijnbraam/megapixels) is currently the best camera application for the PinePhone and is GTK based,
* [NeoChat](https://invent.kde.org/network/neochat) is a native chat client for matrix, the decentralized communication protocol,
* [Okular](https://invent.kde.org/graphics/okular) is a document viewer, support PDF and other document formats,
* [Phone](https://invent.kde.org/plasma-mobile/plasma-dialer) is the app you use to place calls,
* [Phone Book](https://invent.kde.org/plasma-mobile/plasma-phonebook) is the phone book app, where you manage your contacts,
* _Psensor_ is a GTK-based temperature manager, tracking the temperatures of the PinePhone &mdash; you're most likely not going to need it,
* [Recorder](https://invent.kde.org/plasma-mobile/krecorder) is a simple audio recorder,
* [Settings](https://invent.kde.org/plasma-mobile/plasma-settings) is the app where you can change your lockscreen PIN, set your local language and adjust time and date, set up synchronisation with your OwnCloud, Nextcloud or Google account, and hopefully set up your APN for a mobile internet connection too,
* [Spacebar](https://invent.kde.org/plasma-mobile/spacebar) is the SMS application for Plasma Mobile,
* [Telegram](https://desktop.telegram.org/) is the Telegram Desktop, which you may know from your desktop. It works surprisingly well on mobile!
* [Terminal](https://invent.kde.org/plasma-mobile/qmlkonsole) is your terminal app &mdash; this is a Linux Phone after all &mdash;
* [vvave](https://invent.kde.org/maui/vvave/) is a music player and
* [Weather](https://invent.kde.org/plasma-mobile/kweather) is the very nice Plasma Mobile weather app.

Further apps that you might want to  install:
* [Kube](https://kube-project.com/) is a "communication and collaboration" app, alternatively go for _Geary_ (make sure to search for Geary Mobile, as the other variant that I installed in my video does not work properly on the PinePhone).
* [Password Safe](https://gitlab.gnome.org/World/PasswordSafe), a KeePass compatible password manager which while native on GNOME, works fine on Plasma Mobile too – the file picker is a bit hard to use, but you can double tap to load your password database.
* Firefox with mobile-config-firefox, to have another browser. Search `mobile-config-firefox` in Discover (you need to scroll all the way down on the results page, just like with geary-mobile).
* [Pure Maps](https://rinigus.github.io/pure-maps/) is a great maps and navigation app. Install is going to take a little longer, as it is only available via Flathub currently.

If you need more apps, please check out [my recent blog post about my setup](https://linmob.net/2021/01/09/my-setup-with-danctnix-archlinuxarm.html) and make sure to search [LINMOBapps](https://linmobapps.frama.io), and remember: _Kirigami_ apps are preferred, as they are native to Plasma Mobile. Some of them may just be available on the [Arch User Repository](https://aur.archlinux.org), see this [blog post](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html) for more information on how to use it. Also note that you can add widgets to the home screen by pressing long on it.

If you haven't done that yet, make sure to watch my video on 
* [PeerTube](https://www.youtube.com/watch?v=1uUMiNS5rlw),
* [LBRY](https://lbry.tv/@linmob:3/getting-started-with-the-kde-community:1)
* or [YouTube](https://devtube.dev-wiki.de/videos/watch/e32832c3-55e5-4483-b906-5164be4c7839).

Feel free to get in touch via Twitter, Mastodon or email if you have further questions!


[^1]: I am focussing on Manjaro as it's close to the development while still working for the average user. postmarketOS Edge is tracking released software (5.20, 5.21 etc.) and just don't try KDE Neon for the PinePhone – it breaks. 
[^2]: "PinePhone: Manjaro Plasma Mobile (soon on the KDE Community Edition)", [PeerTube](https://devtube.dev-wiki.de/videos/watch/5c73aab1-cb41-4354-81fe-a56a36324806), [LBRY](https://lbry.tv/@linmob:3/pinephone-manjaro-plasma-mobile-soon-on:d), [YouTube](https://www.youtube.com/watch?v=eJ8V0lRxKgM).
