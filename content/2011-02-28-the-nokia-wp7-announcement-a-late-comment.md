+++
title = "The Nokia WP7 announcement: A late comment."
aliases = ["2011/02/28/the-nokia-wp7-announcement-a-late-comment.html"]
author = "peter"
date = "2011-02-28T10:18:00Z"
layout = "post"
[taxonomies]
tags = ["Intel Atom Medfield", "MeeGo", "microsoft", "Nokia", "Nokia N950", "Nokiasoft", "Qt", "symbian foundation"]
categories = ["software", "commentary"]
authors = ["peter"]
+++

_While everybody else is preparing for CeBIT, which I will attend, too (though just for one day) I want to spend some time on pointing out my opinion on Nokia's WP7 deal._

Knowing not too much about Symbian (I actually never head a Symbian cell phone / smart phone), but having watched the evolvement of mobile platforms during the past years, this announcement saddened me when I heard of it. Not just because I dislike Microsoft (I admire them for their success, but believe that their market domination is not a good thing), much more because I am supportive of open source software.

While Nokia's previous MeeGo / Symbian strategy had a common factor (Qt) and, as now leaked out, were to get about the same user interface / UX, which meant a huge commitment to opensource software due to the opensourced nature of Symbian and the fact that MeeGo is Linux based and Qt is an opensource toolkit, originally developed by a company called Trolltech, this announcement put an end to this, as Nokia officials stated that Qt wouldn't be ported over to WP7 and as Microsoft doesn't allow GPLv3 (or similar) licensed software on its relatively new (and thus relatively immature) mobile platform, this meant a slap in the face of the open source community. Nokia will release a developer aimed follow up to the Maemo5 running N900 this year though, most likely this phone will be called N950 and be a keyboardless, touchscreen only device, running MeeGo&mdash;probably on silicon by Intel (Medfield). 

The move towards WP7, including the dumping of Symbian and making MeeGo a &#8220;playground for future research&#8221;, didn&#8217;t come as too much of a surprise. Nokia's smartphone smartphone sales continued to drop down, and a while ago the MeeGo lead at Nokia, <a href="http://jaaksi.blogspot.com/">Ari Jaaski</a>, left after a new CEO was in charge: <a href="http://en.wikipedia.org/wiki/Stephen_Elop">Steven Elop</a>, who had worked at Microsoft before. Then, only a few days before Nokia announced its new partnership, Symbian^4 was canceled with Nokia saying, that they would stop the versioning of Symbian (which isn't the worst idea ever, especially if you failed to fulfill reviewers expectations before, as major releases raise expectations)&mdash;it all sounded strange, and the WP7 rumors became more and more powerful.

Eldar Murtazin, editor in chief of <a href="http://www.mobile-review.com/index-en.shtml">mobile-review.com</a>, a russia-based website which has been reviewing mobile phones (smartphones and &#8220;normal ones&#8221;) for ages (in a very good and detailed way, better than all german magazines on this very topic I ever sneaked a look at), has written a few very<a href="http://mobile-review.com/articles/2011/nokia-microsoft-en.shtml"> interesting</a> <a href="http://mobile-review.com/articles/2011/birulki-103-en.shtml#2">articles</a> about this move (which he doesn't consider a wise one) which features data that is quite interesting: The numbers of employees at Nokia and what they are assigned to.  Looking at these numbers, 6200 people working on Symbian (kernel + UI / UX) in comparison to what reviewers voices were makes you think: Now what do all these girls and guys do?

Actually, it might just be a slow release schedule that makes all these employees seem so lazy, as there will be one more mayor UI/UX overhaul for Symbian later this year&mdash;if you wonder, why Nokia does this, just look at the scheduled release of their first WP7 phone: It won't be on the market that soon (christmas?), so Nokia has to keep Symbian alive to sell at least some handsets&mdash;and they do so without too many tears and crying, as Symbian development is being generously supported by the European Union (when I read about this for the first time, I only thought: What the h***?).

Many people have been asking what will happen to Qt when Nokia has killed (the previously tax payer funded) Symbian in 2013 or 2014 and MeeGo just remains a playground for R&amp;D and a few others at Nokia&mdash;I don't know the answer, but as this is a widely successful technology, I doubt that Nokia will dump it to the trash bin&mdash;they'll much rather try to sell it, as this transition will be a costly process&mdash;and I am sure they will find a buyer, maybe Intel, as MeeGo relies on Qt quite heavily.  

All in all, this seems to be a sad move away from what sounded to be a promising idea: Symbian and MeeGo based smartphones with alike Qt based UI/UX&mdash;it's a move that happens before delivery. Nokia will face hard times with WP7 just as it would have with MeeGo/Symbian, it's doubtable whether this move helps Nokia to bring down cost as they will have to pay a few US$ for each and every license. 

The winner of this move is _Microsoft_, as Nokia becomes the most committed WP7 handset manufacturer, and they gain access to Nokia's OviMaps (some people bought Symbian phones just because of the free offline navigation Nokia offers)&mdash;Nokia remains struggling, just as it has been before.
