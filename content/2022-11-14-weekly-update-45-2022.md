+++
title = "Weekly GNU-like Mobile Linux Update (45/2022): Phosh 0.22.0 and Voice Controlled Sxmo"
date = "2022-11-14T22:45:00Z"
updated = "2022-11-16T16:11:00Z"
draft = false
[taxonomies]
tags = ["Waydroid","Sxmo","Sailfish OS","Librem 5","Phosh","FOSDEM 2023",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " with contributions by ollieparanoid and friendly assistance from plata's awesome script"
update_note = "2022-11-15: Added two items to worth noting that were forgotten initially. 2022-11-16: Fixed Sxmo voice control URL"
+++

Also: Guides to updating u-boot and getting Waydroid running on the Librem 5, FOSDEM 2023 FOSS on mobile devices devroom CfP, the Juno Tablet on video, and improvements to Sailfish OS on the PinePhone Pro!
<!-- more -->

_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#69 Zapping Through Videos](https://thisweek.gnome.org/posts/2022/11/twig-69/)
- Jakub Steiner: [Running Shell in Builder](https://blog.jimmac.eu/2022/Builder-Shell/)
- Jonathan Blandford: [Crosswords 0.3.6: “The Man from G.N.O.M.E”](https://blogs.gnome.org/jrb/2022/11/07/crosswords-0-3-6-the-man-from-g-n-o-m-e/) _Ctrl+F mobile 🚀_
- Phosh 0.22.0 was released ([full release notes](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.22.0)) alongside new releases of [phosh-mobile-settings and phosh-osk-stub](https://social.librem.one/@agx/109303760484130764).

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: better environment variable support](https://pointieststick.com/2022/11/11/this-week-in-kde-better-environment-variable-support/)
- KDE Announcements: [KDE Ships Frameworks 5.100.0](https://kde.org/announcements/frameworks/5/5.100.0/)
- Volker Krause: [Push Notifications for KDE](https://www.volkerkrause.eu/2022/11/12/kde-unifiedpush-push-notifications.html)
- KDE Announcements: [KDE Plasma 5.26.3, Bugfix Release for November](https://kde.org/announcements/plasma/5/5.26.3/)
- Qt blog: [Qt Creator 9 RC released](https://www.qt.io/blog/qt-creator-9-rc-released)

#### Sailfish OS
* [Adam Pigg on Twitter: "Latest improvements to #sailfishos on the @thepine64 #pinephonepro include kernel 6.0.7, fixed als/prox, fixed firmware loading, usb detection including mtp mode and network fixes."](https://twitter.com/adampigg/status/1591574409697009665)
* [Holger: "Whisperfish, an open-source Signal client for SailfishOS, can now be coupled as a secondary device. Which means I  can finally use both my mobile and my \[Android\] tablet" - Functional Café](https://functional.cafe/@schaueho/109304397565914010)

#### Ubuntu Touch
* [Codemaster Freders on Twitter: "Opinions welcome, how does All-Blur-Everything in the Lomiri indicator bar look to you?"](https://twitter.com/fredldotme/status/1591125732268593158)

#### Distributions
- Kupfer: [v0.1.5, Preparing for v0.2.0rc0](https://kupfer.gitlab.io/blog/2022-11-09_v0-1-5_preparing_for_v0-2.html)
- Kupfer: [v0.2.0rc0 merged to dev](https://kupfer.gitlab.io/blog/2022-11-12_v0-2-0rc0_merged.html)
- Phoronix: [Fedora 38 Looking At A Phosh Image For Mobile Devices](https://www.phoronix.com/news/Fedora-38-Phosh-Proposal)
- [CutiePi tablet on Twitter: "And of course, Ubuntu 22.10 image is ready Download"](https://twitter.com/cutiepi_io/status/1589041918243229697)

#### Kernel
- Phoronix: [Rust Developers Move Ahead With Preparing To Upstream More Code Into The Linux Kernel](https://www.phoronix.com/news/More-Rust-Upstream-Prep-Linux)
- Phoronix: [Linux 6.1-rc5 Released - This Kernel May Need An Extra Week To Bake](https://www.phoronix.com/news/Linux-6.1-rc5)

#### Stack 
- Phoronix: [wlroots 0.16 Released With More Stable Vulkan Renderer, High Resolution Scrolling](https://www.phoronix.com/news/wlroots-0.16-Released)
- [Guido Günther: "🚀📱 I've tagged version 0.0.1 of #feedbackd (a dae…"](https://social.librem.one/@agx/109325232552434083)

#### Matrix
- Matrix.org: [This Week in Matrix 2022-11-11](https://matrix.org/blog/2022/11/11/this-week-in-matrix-2022-11-11)

### Worth noting
* FOSDEM 2023, FOSS on mobile devices devroom: [Call for Papers](https://gitlab.com/fosdem_mobile/devroom/-/blob/main/README.md) is out! _I'll likely attend - travel and accomodation is booked!_
* Have a Librem 5 and want to update u-boot? [Here's how!](https://fosstodon.org/@samuelnorbury/109335519465603008)
* Have a Librem 5 and want to run Waydroid in order to use the few Android apps you can't just replace? [Sebastian Krzyszkowiak not only made Waydroid run well, he also wrote up how to use it on the Librem 5.](https://source.puri.sm/-/snippets/1198) _The snippet also contains a helpful [Tips & tricks](https://source.puri.sm/-/snippets/1198#tips-tricks) section that non-Librem 5 Waydroid users may find helpful, too!_
* Have a Librem 5 and modem troubles? [This may help.](https://chrichri.ween.de/o/85810fb146cc414287e5260d11273c1f)[^1]
* Some methods of text input have been lacking on #mobilelinux. [So if you can write e.g. Japanese, Guido built something for you and needs your feedback!](https://social.librem.one/@agx/109326021241539147)[^1]


### Worth reading
- Purism: [Toward Matrix support in Chats](https://puri.sm/posts/toward-matrix-support-in-chats/)
- gondolyr on Purism forums: [Librem 5: My experience after a month of use](https://forums.puri.sm/t/librem-5-my-experience-after-a-month-of-use/18647)

### Worth watching
- proycon: [Sxmo voice control with Numen](https://tube.simple-web.org/diode.zone/videos/watch/690cf4cb-82af-4b18-8827-71b839a2df5b/). _While watching this, I was asked whether I was having a stroke. Impressive, though!_
- CalcProgrammer1: [PinePhone Keyboard RGB - Part 1: PCB Design](https://www.youtube.com/watch?v=9DGIwjaXMeI) _Impressive!_
- Linux Stuff: [Juno Tablet - Manjaro Plasma Mobile](https://www.youtube.com/watch?v=SZO75F9wZvU)
- Linux Stuff: [Juno Tablet - Mobian Phosh - Intro](https://www.youtube.com/watch?v=8mQbQ18tN9A) _Nice!_
- Nicco Loves Linux: [UBUNTU TOUCH STORY's WEIRD!](https://www.youtube.com/watch?v=jmPVftUuH6E)
- Puffercat: [How to install GApps on Sailfish OS Android app support](https://www.youtube.com/watch?v=BTp2SU9nUMw)
- Some Jolla Stuff: [Sailfish OS Ambience bug](https://www.youtube.com/watch?v=y3MSKip0wFg)
- Sailfish official: [google pixel 3a install droidian os](https://www.youtube.com/watch?v=g8fMo6aVejc)
- @alex_m: [Highscore (GNOME Retro Gaming app) on Librem 5](https://mastodon.online/@alex_m/109318632168217377)
- Techonsapevole: [🐧 L'ALTERNATIVA AD ANDROID E IOS. PINEPHONE LINUX MOBILE TOUR MANJARO ARM](https://www.youtube.com/watch?v=lqjZ8OeeYFg) _Italian, but there aren't many walkthroughs these days and automatically translated subtitles somewhat work._

### Thanks

Huge thanks to ollieparanoid for contributing to this installment of the Weekly Update and also to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

PPS: This one has (once again) less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)

[^1]: Due to being tired, these two items weren't part of the initial publication of this Weekly Update.
