+++
title = "Still one A910."
aliases = ["2008/08/still-one-a910.html"]
date = "2008-08-14T17:29:00Z"
[taxonomies]
tags = ["A910", "A910i", "Android", "development", "OpenEZX", "i-geekmod",]
categories = ["hardware", "projects"]
authors = ["peter"]
+++
I bought another one, as you know, and it arrived in a good (but not perfect..) constitution. But I was stupid enough to kill my old one, to prove that you shouldn't flash R58(A910i) firmwares to R57 (A910) devices. (If anyone wants to donate something for my igeekmod work, why not donate an A910 ;))
<!-- more -->
But as I posted on Motorolafans already, I &#8220;finished&#8221; two working flashfiles for A910 yesterday. And igeekmod R2 will take its time to make it really as good as I want it to be. I hope that I can improve the phones speed, though I have no idea on how to do so (anybody?), as it is too slow as it is.

That was the reason for posting all these Android videos, BTW. Let's hope that _WyrM_ and _ao2_ will be successfully spending their time on their A910s &mdash;ANDROID would be really attractive on A910, as it seems to run really fast&mdash;even on relatively under-powered phones.
