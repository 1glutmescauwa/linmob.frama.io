+++
title = "Weekly GNU-like Mobile Linux Update (3/2023): Pinhole photography"
date = "2023-01-23T12:40:00Z"
draft = false
[taxonomies]
tags = ["Sailfish OS", "PinePhone", "nheko", "KDE 6",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

An unofficial PINE64 community update, a library to help Phosh deal with notches, bleak future for Cawbird and experiments with video recording on PinePhone.
<!-- more -->

_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#79 Research Results](https://thisweek.gnome.org/posts/2023/01/twig-79/)
- Allan Day: [gnome-info-collect: What we learned](https://blogs.gnome.org/aday/2023/01/18/gnome-info-collect-what-we-learned/)
- [Sonny: "Tangram 2.0 is out and available on #Flathub…"](https://floss.social/@sonny/109711975307382080)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: The best Plasma 5 version ever](https://pointieststick.com/2023/01/20/this-week-in-kde-the-best-plasma-5-version-ever/)
- Volker Krause: [KDE Frameworks has been branched](https://www.volkerkrause.eu/2023/01/21/kf6-branched.html) _6!_
- KDE Announcements: [Plasma 5.27 Beta](https://kde.org/announcements/plasma/5/5.26.90/)

#### Sailfish OS
- Adam Pigg: [Nothing exciting, except the first photo taken on a @thepine64 #pinephone on #sailfishos using a GUI camera app](https://twitter.com/adampigg/status/1616162198811774979), [Camera app source code](https://github.com/piggz/harbour-pinhole) _Based on libcamera, nice!_

#### Capyloon
- Capyloon: [Fri 20 January 2023](https://capyloon.org/releases.html#jan-20-2023) _PinePhone Pro installer image... I hope I find the time to try this soon!_

#### Distributions
- Phoronix: [Debian 12 "Bookworm" Hits Its First Freeze](https://www.phoronix.com/news/Debian-12-First-Freeze). _The sad thing: As testing freezes, Mobian becomes quite boring. ;-)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-01-20](https://matrix.org/blog/2023/01/20/this-week-in-matrix-2023-01-20)
- Matrix.org: [Synapse 1.75 released](https://matrix.org/blog/2023/01/19/synapse-1-75-released)
- nheko (Matrix): [v0.11.1](https://github.com/Nheko-Reborn/nheko/releases/tag/v0.11.1)

### Worth noting
- [Guido Günther: "I've tagged an initial version 0.0.1 of gmobile, a small supporting library used by #phosh, #feedbackd and soon #phoc for notches/cutouts, timers and the like…"](https://social.librem.one/@agx/109728785384596764)
- MobileLinux: [@calebccff booting mainline on OnePlus 7 Pro](https://www.reddit.com/r/mobilelinux/comments/10d05vm/calebccff_booting_mainline_on_oneplus_7_pro/)
- [Travelling to FOSDEM and have a PineTime?](https://mastodon.codingfield.com/@JF/109733380419238353))
- PINE64 forums: [How can I record video on a Pinephone?](https://forum.pine64.org/showthread.php?tid=16539&pid=115528). _Interesting thread, for more camera news check out the Sailfish OS section above._
- [Cawbird on Twitter: "It's official. We're living on borrowed time. Twitter has updated its Developer Agreement to ban "creating a substitute to the Twitter Applications" (§ II.A.c) In preparation for when we're eventually unplugged: So long, and thanks for all the tweets! https://t.co/FHWTvs3YIQ""](https://twitter.com/CawbirdClient/status/1616507506389356549)
- [PINE64: "#PineTab2 software progress :)…"](https://fosstodon.org/@PINE64/109717500293641604)

### Worth reading
- fossphones.com: [Linux Phone News - January 17, 2023](https://fossphones.com/01-17-23.html). _Well done!_
- mdk.fr: [Black screen at boot on my PinePhone](https://mdk.fr/blog/black-screen-at-boot-on-my-pinephone.html)
- Nico Cartron: [Trying to guess new Sailfish OS features through localisation](https://www.ncartron.org/trying-to-guess-new-sailfish-os-features-through-localisation.html)
- Purism: [What We All Want](https://puri.sm/posts/what-we-all-want/)


### Worth watching
- (RTP) Privacy Tech Tips: [I2P On The Pinephone / Pinetab](https://www.youtube.com/watch?v=JEaJs8A0P6E)
- Linux Lounge: [Pine64 January 2023 Update (Unofficial) - PineBuds FOSS Firmware, PineTab 2 Video Output & More](https://www.youtube.com/watch?v=Umj-w8LM3rw) _Nice!_
- qkall: [Pinephone Pro Arch SXMO - Jan 2023](https://www.youtube.com/watch?v=erFvTeZYMGE)
- Jozef Mlich: [Notifications cleanup](https://www.youtube.com/watch?v=DtvDROS161s) _Nemo Mobile!_
- Lup Yuen Lee: [#LVGL on #PinePhone and #NuttX becomes more Touch-Friendly](https://www.youtube.com/watch?v=JQTh3VTTTkc)
- Lup Yuen Lee: [#LVGL Music Player on #PinePhone on Apache #NuttX RTOS ... Sorry no audio yet](https://www.youtube.com/watch?v=_cxCnKNibtA)
- Lup Yuen Lee: [#LVGL on #PinePhone on Apache #NuttX RTOS ... Increased the Default Font Size from 14 to 20 🤔](https://www.youtube.com/watch?v=N-Yc2jj3TtQ)
- Lup Yuen Lee: [#LVGL Benchmark on #PinePhone on Apache #NuttX RTOS](https://www.youtube.com/watch?v=deBzb-VbHck)
- Lumpology: [Turning my Phone into a Linux PC (Ubuntu Touch)](https://www.youtube.com/watch?v=IMIwYaWgttM). _Impressive!_
- Purism: [What We All Want](https://www.youtube.com/watch?v=2KVau1yKCsU) _Oof._

### Thanks

Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

Also, [help with getting this blog relisted on Bing is appreciated](https://fosstodon.org/@linmob/109726744903010450)

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)
